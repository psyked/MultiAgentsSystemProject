import matplotlib.pyplot as plt

plotTab = []
f = open('buyerLog.txt', 'r')
x = f.readlines()
f.close()
for s in x:
    plotTab.append(float(s[:-1]))

plotTab2 = []
f = open('sellerLog.txt', 'r')
x = f.readlines()
f.close()
for s in x:
    plotTab2.append(float(s[:-1]))

plt.plot(plotTab, color='red')
plt.plot(plotTab2, color='blue')
plt.plot(plotTab2[0]*17, color='red', linestyle='--')
plt.plot(plotTab[0]*17, color='blue', linestyle='--')
plt.ylabel('Offers')
plt.xlabel('Ticks')
plt.show()