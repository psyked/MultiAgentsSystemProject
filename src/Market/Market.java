package Market;

import Agents.AgentMarket;
import Agents.AgentMarketBuyer;
import Agents.AgentMarketSeller;
import jade.wrapper.AgentContainer;
import jade.wrapper.AgentController;
import sun.awt.Mutex;

import java.util.ArrayList;
import java.util.HashMap;

public class Market{
    private Market(){}
    private static Market INSTANCE = new Market();
    private HashMap<Item, AgentMarketSeller> itemsToSell = new HashMap<>();
    private HashMap<Item, AgentMarketBuyer> itemsToBuy = new HashMap<>();
    private ArrayList<AgentMarket> subscribedAgents = new ArrayList<>();
    private ArrayList<Item> transactionItems = new ArrayList<>();
    private Mutex mutexListsAgents = new Mutex();
    private ArrayList<AgentMarket> unsubscribedAgents = new ArrayList<>();
    private Mutex mutexUnsubscrib = new Mutex();
    private Mutex mutexItems = new Mutex();
    private Mutex mutexTransaction = new Mutex();
    private int maxOfferPerTick = 100;
    private int timePerTick = 100;
    private boolean waitingTicker = false;
    Ticker agentTicker = null;

    private int previousTick = 0;

    public static Market getInstance()
    {
        return INSTANCE;
    }

    public boolean setTicker(Ticker agentTicker)
    {
        if(this.agentTicker != null)
            return false;
        this.agentTicker = agentTicker;
        previousTick = agentTicker.getTicks();

        return true;
    }

    public void setController(AgentContainer container)
    {
        mutexListsAgents.lock();
        if(agentTicker == null && !waitingTicker)
        {
            try
            {
                waitingTicker = true;
                AgentController agent = container.createNewAgent("ticker", "Market.Ticker", new Object[]{});
                agent.start();
            }catch (Exception e)
            {
                waitingTicker = false;
                System.out.println("Can't create ticker");
                e.printStackTrace();
            }
        }
        mutexListsAgents.unlock();
    }
    public void NextTick()
    {
        //System.out.println("tick : " + previousTick);

        if(agentTicker == null)
            return;
        if(previousTick >= agentTicker.getTicks())
            return;
        previousTick = agentTicker.getTicks();
        mutexListsAgents.lock();
        subscribedAgents.forEach(AgentMarket::nexTick);
        mutexUnsubscrib.lock();
        unsubscribedAgents.forEach(this::removeAgent);
        unsubscribedAgents.clear();
        mutexUnsubscrib.unlock();
        mutexListsAgents.unlock();
    }
    private void removeAgent(AgentMarket agent)
    {
        subscribedAgents.remove(agent);
        ArrayList<Item> itemsToRemove = new ArrayList<>();
        if(agent.getClass() == AgentMarketBuyer.class)
        {
            mutexItems.lock();
            itemsToSell.forEach((item, sel)->
            {
                if(sel == agent)
                {
                    itemsToRemove.add(item);
                }
            });
            itemsToRemove.forEach(item->
            {
                itemsToSell.remove(item);
            });
            mutexItems.unlock();
        }
        else if(agent.getClass() == AgentMarketSeller.class)
        {
            mutexItems.lock();
            itemsToBuy.forEach((item, sel)->
            {
                if(sel == agent)
                {
                    itemsToRemove.add(item);
                }
            });
            itemsToRemove.forEach(item->
            {
                itemsToBuy.remove(item);
            });
            mutexItems.unlock();
        }
    }

    public int getCurrentTick() {
        if(agentTicker==null)
            return 0;
        return agentTicker.getTicks();
    }

    public int getTimePerTick() {
        return timePerTick;
    }

    public int getMaxOfferPerTick() {
        return maxOfferPerTick;
    }

    public ArrayList<AgentMarketSeller> FindSellersMatchingItem(Item item)
    {
        mutexItems.lock();
        ArrayList<AgentMarketSeller> agents = new ArrayList<>();
        itemsToSell.forEach((it, seller)->
        {
            if(it.Match(item))
            {
                agents.add(seller);
            }
        });
        mutexItems.unlock();
        return (agents.size()<=0)? null : agents;
    }
    public ArrayList<AgentMarketSeller> AddNewItemToBuy(Item item, AgentMarketBuyer agent)
    {
        SubscribeAgent(agent);
        mutexItems.lock();
        itemsToBuy.put(item, agent);
        mutexItems.unlock();
        return FindSellersMatchingItem(item);
    }
    public void RemoveNewItemToBuy(Item item)
    {
        mutexItems.lock();
        itemsToBuy.remove(item);
        mutexItems.unlock();
    }
    public void SubscribeAgent(AgentMarket agent)
    {
        mutexListsAgents.lock();
        if(!subscribedAgents.contains(agent))
        {
            subscribedAgents.add(agent);
        }
        mutexListsAgents.unlock();
    }
    public void UnSubscribeAgent(AgentMarket agent)
    {
        mutexUnsubscrib.lock();
        unsubscribedAgents.add(agent);
        mutexUnsubscrib.unlock();
    }
    public void RemoveNewItemToSell(Item item)
    {
        mutexItems.lock();
        itemsToSell.remove(item);
        mutexItems.unlock();
    }
    public void AddNewItemToSell(Item item, AgentMarketSeller seller) {
        SubscribeAgent(seller);
        mutexItems.lock();
        itemsToSell.put(item, seller);
        itemsToBuy.forEach((it, buyer)->
        {
            if(it.Match(item))
                buyer.NewSellerEvent(seller, item);
        });
        mutexItems.unlock();
    }

    public boolean FinishTransaction(AgentMarket target, AgentMarket client, Item item, boolean isSeller)
    {
        mutexTransaction.lock();
        transactionItems.add(item);
        boolean returnedValue = target.ReceiveAcceptedOffer(item, client, !isSeller);
        transactionItems.remove(item);
        System.out.println(new StringBuilder().append(client.getName()).append(" to ")
                .append(target.getName()).append(item.getName()).append(" in ").append(item.getQuantity())
                .append(" for ").append(item.getPrices()[0]).append(returnedValue).append("\n"));
        mutexTransaction.unlock();
        return returnedValue;
    }
    public boolean VerificationTransaction(Item item)
    {
        return transactionItems.contains(item);
    }
}
