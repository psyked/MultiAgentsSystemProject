package Market;

import java.time.Instant;
import java.util.ArrayList;
import java.util.Date;

public class Item {
    String name;
    Date dateLimite;
    Date dateCreation;
    int tickLimite;
    int tickCreation = -1;
    ArrayList<String> filters; // hasmap<string, object> ?
    int quantity;
    double[] prices = new double[2];

    public Item(String n, Date lim, double[] p, int q, ArrayList<String> f)
    {
        this(n, p, q, f);
        dateLimite = lim;
        dateCreation = Date.from(Instant.now());
    }

    public Item(String n, int nbTicks, double[] p, int q, ArrayList<String> f)
    {
        this(n, p, 1, new ArrayList<String>());
        tickCreation = Market.getInstance().getCurrentTick();
        tickLimite = nbTicks;
    }
    public Item(String n, double[] p, int q, ArrayList<String> f)
    {
        name = n;
        prices = p;
        filters = f;
        quantity = q;
    }
    public Item Clone()
    {
        Item i;
        if(tickCreation >= 0)
            i = new Item(name, tickLimite, prices.clone(), quantity, (ArrayList<String>)filters.clone());
        else
            i = new Item(name, (Date) dateLimite.clone(), prices.clone(), quantity, (ArrayList<String>) filters.clone());
        i.dateCreation = this.dateCreation;
        i.tickCreation = tickCreation;
        return i;
    }
    // Return the percentage of completion (limite - creation)
    public float getCompletion()
    {
        double fullC;
        double currentC;
        if(tickCreation >= 0)
        {
            fullC = tickLimite;
            currentC = (Market.getInstance().getCurrentTick() - tickCreation);
        }
        else
        {
            fullC = dateLimite.getTime() - dateCreation.getTime();
            currentC = Date.from(Instant.now()).getTime() - dateCreation.getTime();
        }

        return (float)(currentC / fullC);
    }

    public ArrayList<String> getFilters() {
        return filters;
    }

    public Date getDateLimite() {
        return dateLimite;
    }

    public String getName() {
        return name;
    }

    public Date getDateCreation() {
        return dateCreation;
    }

    public int getQuantity() {
        return quantity;
    }
    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }
    public void addQuantity(int q)
    {
        if(q > 0)
            quantity += q;
    }
    public void removeQuantity(int q)
    {
        if(quantity > q && q > 0)
            quantity -= q;
    }
    public boolean Match(Item item)
    {
        if(!name.equals(item.name))
            return false;
        boolean canContinue =
                filters.stream().anyMatch(filter-> !item.filters.contains(filter));
        return !canContinue;
    }
    public double[] getPrices() {
        return prices;
    }

    public void setPriceStart(double price) {
        this.prices[0] = price;
    }
}
