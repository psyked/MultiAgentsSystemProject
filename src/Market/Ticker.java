package Market;

import jade.core.Agent;
import jade.core.behaviours.TickerBehaviour;

public class Ticker extends Agent {

    int ticks = 0;
    public int getTicks()
    {
        return ticks;
    }

    @Override
    protected void setup() {
        Market market = Market.getInstance();
        System.out.println("Agent "+getLocalName()+" is up");
        if(!market.setTicker(this))
        {
            doDelete();
            return;
        }
        addBehaviour(new TickerBehaviour(this, market.getTimePerTick()) {
            protected void onTick() {
                ticks = getTickCount();
                //System.out.println("Agent "+myAgent.getLocalName()+": tick="+ ticks);
                market.NextTick();
            }
        });
        /*
        try {
            for(int i = 0; i < 10; i++)
            {
                getContainerController().createNewAgent("seller"+i, "Agents.AgentMarketSeller", new Object[]{}).start();
                getContainerController().createNewAgent("buyer"+i, "Agents.AgentMarketBuyer", new Object[]{}).start();
            }
        }catch (Exception e)
        {
            System.out.println("fail to create test seller or buyer");
            e.printStackTrace();
        }*/
    }
}
