package Agents;

import Market.Item;
import Market.Market;

import java.util.ArrayList;

public class AgentMarketSeller extends AgentMarket {
    protected void setup()
    {
        market.setController(getContainerController());
        System.out.println("Agent " + getLocalName() + " is configured");
        Initialisation(getArguments());
    }
    @Override
    public void Initialisation(Object[] args) {
        // todo : remove after tests
        itemsInventory.add(new Item("toto", 20, new double[]{12,10}, 1, new ArrayList<String>()));

        itemsInventory.forEach(item->
        {
            market.AddNewItemToSell(item,this);
        });
    }
    @Override
    public void Offer(Item item, AgentMarket buyer) {
    {
      TestOffer(item, buyer, false);
    }}
    @Override
    public boolean TestOffer(Item item, AgentMarket buyer, boolean testOffer) {
        //System.out.println(getLocalName() + " is offered " + item.getPrices()[0] + " for " + item.getQuantity() + " units of " + item.getName());

        if(checkNumberMatchingItems(item, itemsInventory))
            return false;

        ArrayList<Item> itemsFiltered = filterItems(item, itemsInventory);
        itemsFiltered.sort((a, b) -> (calculateActualPriceItem(a) < calculateActualPriceItem(b)) ? 1 : 0);
        double moy = calculateMeanPrice(itemsFiltered, item.getQuantity());
        if(moy <= item.getPrices()[0])
        {
            if(testOffer)
                return true;
            item.setPriceStart(item.getPrices()[0]);
            AcceptOffer(buyer, item, true);
            return true;
        }
        if(testOffer)
            return false;

        createNewOffer(item, itemsFiltered);
        this.nextTransactions.put(buyer, item);
        return false;
    }

    @Override
    protected void createNewOffer(Item item, ArrayList<Item> itemsSet) {
        itemsSet.sort((a, b) -> (calculateActualPriceItem(a) < calculateActualPriceItem(b)) ? 1 : 0);
        int quantity = (getNumberOfMatchingItems(item, itemsInventory) < item.getQuantity())?
                getNumberOfMatchingItems(item, itemsInventory) : item.getQuantity();
        double moy = calculateMeanPrice(itemsSet, quantity);
        item.setPriceStart(moy);
        item.setQuantity(quantity);
    }
    protected void removeItemsToSell(ArrayList<Item> itemsSet)
    {
        itemsSet.forEach(item->
        {
            itemsInventory.remove(item);
            Market.getInstance().RemoveNewItemToSell(item);
        });
    }

    @Override
    protected boolean canEnd() {
        ArrayList<Item> itemsToBeDeleted = new ArrayList<>();
        itemsInventory.forEach(item->
        {
            if(item.getCompletion() > 1)
            {
                itemsToBeDeleted.add(item);
                log.append("item : ").append(item.getName()).append(" can not be sold anymore (transaction time overdue).\n");
                System.out.println("deleting item from inventory for time overdue " + item.getCompletion());
            }
        });
        removeItemsToSell(itemsToBeDeleted);

        if(itemsInventory.size() <= 0)
            return true;
        return false;
    }

    @Override
    protected void taskEnded() {
        super.taskEnded();
        System.out.println("Agent " + getLocalName() + " ends");

        // if(activLog) System.out.println(log);
        market.UnSubscribeAgent(this);
        doDelete();
    }


    @Override
    protected double calculateActualPriceItem(Item item) {
        double actualPrice = item.getPrices()[0];
        actualPrice -= CalculateDifferenceThroughTimeWithCurves(item.getCompletion(), actualPrice, item.getPrices()[1]);
        return (actualPrice < item.getPrices()[1])? item.getPrices()[1] : actualPrice;
    }
}
