package Agents;

import Market.Item;
import Market.Market;
import java.util.ArrayList;

public class AgentMarketBuyer extends AgentMarket
{
    protected void setup()
    {
        market.setController(getContainerController());
        System.out.println("Agent " + getLocalName() + " is configured");
        Initialisation(getArguments());
    }

    public void Initialisation(Object[] args)
    {
        // todo : remove after tests
        itemsInventory.add(new Item("toto", 20, new double[]{10,12}, 1, new ArrayList<String>()));
        try {
            getContainerController().createNewAgent("seller", "Agents.AgentMarketSeller", new Object[]{}).start();

        }catch (Exception e)
        {
            System.out.println("fail to create test seller ");
            e.printStackTrace();
        }



        itemsInventory.forEach(item->
        {
            ArrayList<AgentMarketSeller> sellers = market.AddNewItemToBuy(item,this);
            if(sellers == null || sellers.size() <= 0)
                return;
            sellers.forEach(agent->this.nextTransactions.put(agent, item.Clone()));
        });
    }
    public void NewSellerEvent(AgentMarket agent, Item item)
    {
        System.out.println("event : " + agent.getLocalName());
        ArrayList<Item> itemsFiltered = filterItems(item, itemsInventory);
        if(itemsFiltered.size() <= 0)
            return;

        itemsFiltered.forEach(it->this.nextTransactions.put(agent, it.Clone()));
    }
    // verify if the agent can stop (Ex: all item sold, no more money to buy items, etc.)
    protected boolean canEnd()
    {
        ArrayList<Item> itemsToBeDeleted = new ArrayList<>();
        itemsInventory.forEach(item->
        {
            if(item.getCompletion() > 1)
            {
                itemsToBeDeleted.add(item);
                log.append("item : ").append(item.getName()).append(" can not be bough anymore (transaction time overdue).\n");
                System.out.println("deleting item from inventory for time overdue " + item.getCompletion());
            }
        });
        removeItemsToBuy(itemsToBeDeleted);

        if(itemsInventory.size() <= 0)
            return true;
        // stop if not enough money to buy another item
        boolean canContinue =
            itemsInventory.stream().anyMatch(item-> this.getMoney() > item.getPrices()[0]);
        return !canContinue;
    }
    protected void removeItemsToBuy(ArrayList<Item> itemsSet)
    {
        itemsSet.forEach(item->
        {
            itemsInventory.remove(item);
            Market.getInstance().RemoveNewItemToBuy(item);
        });
    }
    @Override
    protected void taskEnded()
    {
        super.taskEnded();
        System.out.println("Agent " + getLocalName() + " ends");
        // display list of objects not used sell/buy
        itemsInventory.forEach(item ->
        {
            StringBuilder sb = new StringBuilder();
            if(item.getQuantity() <= 0)
            {
                sb.append(item.getName()).append(" sold");
            }
            else
            {
                sb.append(item.getQuantity()).append(" items of ")
                        .append(item.getName()).append(" not sold");
            }
            System.out.println(sb.toString());
        });
        itemsRemoved.forEach(item ->
        {
            StringBuilder sb = new StringBuilder();
            if(item.getQuantity() <= 0)
            {
                sb.append(item.getName()).append(" bough");
            }
            else
            {
                sb.append(item.getQuantity()).append(" items of ")
                        .append(item.getName()).append(" bough");
            }
            System.out.println(sb.toString());
        });
        // if in debug, throw historic
        market.UnSubscribeAgent(this);
        doDelete();
    }
    protected double calculateActualPriceItem(Item item)
    {
        double actualPrice = item.getPrices()[0];
        actualPrice += CalculateDifferenceThroughTimeWithCurves(item.getCompletion(), actualPrice, item.getPrices()[1]);
        return (actualPrice > item.getPrices()[1])? item.getPrices()[1] : actualPrice;
    }

    @Override
    public void Offer(Item item, AgentMarket seller)
    {
        TestOffer(item, seller, false);
    }
    @Override
    public boolean TestOffer(Item item, AgentMarket seller, boolean testOffer)
    {
        //System.out.println(getLocalName() + " is offered " + item.getPrices()[0] + " for " + item.getQuantity() + " units of " + item.getName());

        if(checkNumberMatchingItems(item, itemsInventory))
            return false;
        ArrayList<Item> itemsFiltered = filterItems(item, itemsInventory);
        itemsFiltered.sort((a, b) -> (calculateActualPriceItem(a) > calculateActualPriceItem(b)) ? 1 : 0);
        double moy = calculateMeanPrice(itemsFiltered, item.getQuantity());
        if(moy >= item.getPrices()[0])
        {
            if(testOffer)
                return true;
            item.setPriceStart(item.getPrices()[0]);
            AcceptOffer(seller, item, false);
            return true;
        }
        if(testOffer)
            return false;

        createNewOffer(item, itemsFiltered);
        this.nextTransactions.put(seller, item);
        return true;
    }
    protected void createNewOffer(Item item, ArrayList<Item> itemsSet)
    {
        itemsSet.sort((a, b) -> (calculateActualPriceItem(a) < calculateActualPriceItem(b)) ? 1 : 0);
        int quantity = (getNumberOfMatchingItems(item, itemsInventory) < item.getQuantity())?
                getNumberOfMatchingItems(item, itemsInventory) : item.getQuantity();
        double moy = calculateMeanPrice(itemsSet, quantity);
        item.setPriceStart(moy);
        item.setQuantity(quantity);
    }
}
