package Agents;

import Market.Curves;
import Market.Item;
import Market.Market;
import jade.core.Agent;
import sun.awt.Mutex;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.util.ArrayList;
import java.util.HashMap;

public abstract class AgentMarket extends Agent {
    private double money = 100d;
    private Mutex mutexInventory = new Mutex();
    protected Market market = Market.getInstance();
    protected Curves curveType = Curves.cubic;
    StringBuilder log = new StringBuilder();
    protected HashMap<AgentMarket, Item> nextTransactions = new HashMap<>();
    protected ArrayList<Item> itemsInventory = new ArrayList<>();
    protected ArrayList<Item> itemsRemoved = new ArrayList<>();

    public void nexTick()
    {
        int[] max = {Market.getInstance().getMaxOfferPerTick()};
        nextTransactions.forEach((key, val) ->
        {
            if(key == null || val == null)
                return;
            System.out.println(getLocalName() + " offers " + val.getPrices()[0] + " for " + val.getQuantity() + " units of " + val.getName() + " to " + key.getLocalName());
            if (max[0]-- > 0)
                key.Offer(val, this);
        });
        nextTransactions.clear();
        if(canEnd())
            taskEnded();
    }

    public abstract void Offer(Item item, AgentMarket agent);
    public abstract boolean TestOffer(Item item, AgentMarket agent, boolean testOffer);
    public abstract void Initialisation(Object[] args);
    protected abstract boolean canEnd();
    protected void taskEnded()
    {
        try
        {
            BufferedWriter writer = new BufferedWriter(new FileWriter("Logs/" + getLocalName()+"Log.txt"));
            writer.write(log.toString());

            writer.close();
        }catch(Exception e)
        {
            System.out.println("agent " + getLocalName() + " can't write logs");
            e.printStackTrace();
        }
    };
    protected abstract void createNewOffer(Item item, ArrayList<Item> itemsSet);

    protected abstract double calculateActualPriceItem(Item item);
    protected double calculateMeanPrice(ArrayList<Item> itemsSet, int quantity)
    {
        double[] nb = {quantity};
        double[] moy = {0d};
        itemsSet.forEach(it ->{
            if(nb[0] <= 0)
                return;
            double q = (it.getQuantity() < nb[0])? it.getQuantity() : nb[0];
            nb[0] -= q;
            moy[0] += q * calculateActualPriceItem(it);
        });
        return moy[0] /= quantity;
    }
    // check and correct number of needed items
    boolean checkNumberMatchingItems(Item item, ArrayList<Item> itemsSet)
    {
        log.append(item.getPrices()[0]).append("\n");
     //   log.append("Getting offer ").append(item.getPrices()[0]).append("for : ").append(item.getQuantity())
        //        .append(" units of ").append(item.getName()).append("\n");
        int nbMI = getNumberOfMatchingItems(item, itemsSet);
        if(nbMI <= 0)
        {
            log.append("No item available after filter").append('\n');
            return true;
        }
        if(nbMI < item.getQuantity())
        {
            log.append("Not enough item, only ").append(getNumberOfMatchingItems(item, itemsSet))
                    .append(" available, rewrite offer.").append('\n');
            item.setQuantity(nbMI);
        }
        return false;
    }
    int getNumberOfMatchingItems(Item item, ArrayList<Item> itemsSet)
    {
        ArrayList<Item> filteredItems = filterItems(item, itemsSet);
        final int[] nb = {0};
        filteredItems.forEach(i-> nb[0] += i.getQuantity());
        return nb[0];
    }
    protected final ArrayList<Item> filterItems(Item item, ArrayList<Item> itemsSet)
    {
        ArrayList<Item> itemsF = new ArrayList<>();
        itemsSet.forEach(i->{
            if(i.Match(item))
                itemsF.add(i);
        });
        return itemsF;
    }
    // returns the absolute value between two prices with this.curveType at the moment of 'completion'
    protected double CalculateDifferenceThroughTimeWithCurves(float completion, double price1, double price2)
    {
        double diffP = Math.abs(price1 - price2);
        switch (curveType)
        {
            case linear:
                diffP *= completion;
                break;
            case quadratic:
                diffP *= (completion * completion);
                break;
            default:
                diffP *= (completion * completion * completion);
        }
        return diffP;
    }

    public double getMoney() {
        return money;
    }
    public final boolean hasEnoughFounds(double price)
    {
        if(price < money)
        {
            money -= price;
            return true;
        }
        return false;
    }
    private void saveItem(Item item) {
        ArrayList<Item> itemsFiltered = filterItems(item, itemsInventory);
        itemsFiltered.sort((a, b) -> (calculateActualPriceItem(a) < calculateActualPriceItem(b)) ? 1 : 0);
        int[] quantity = {item.getQuantity()};
        ArrayList<Item> itemsToRemove = new ArrayList<>();
        itemsFiltered.stream().anyMatch(it->
        {
            if(quantity[0]<=0)
                return true;
            if(it.getQuantity() <= quantity[0])
            {
                itemsToRemove.add(it);
                itemsRemoved.add(it);
                quantity[0] -= it.getQuantity();
            }
            else
            {
                Item newItem = it.Clone();
                newItem.setQuantity(quantity[0]);
                it.setQuantity(it.getQuantity() - quantity[0]);
                itemsRemoved.add(newItem);
                quantity[0] = 0;
            }
            return false;
        });

        itemsToRemove.forEach(itemsInventory::remove);
    }
    protected final void AcceptOffer(AgentMarket agent, Item item, boolean isSeller)
    {
        System.out.println(getLocalName() + " accept offer " + item.getPrices()[0] + " for " + item.getQuantity() + " units of " + item.getName() + " from " + agent.getLocalName());

        mutexInventory.lock();
        if(!isSeller && !hasEnoughFounds(item.getQuantity() * item.getPrices()[0]))
        {
            mutexInventory.unlock();
            System.out.print("no founds :(");
            return;
        }
        // verif if ressources are available
        if(!TestOffer(item, agent, true))
        {
            System.out.print("not enought items");

            mutexInventory.unlock();
            return;
        }
        // if ok, send the deal to the seller,
        if(!Market.getInstance().FinishTransaction(agent, this, item, isSeller))
        {
            System.out.println(getLocalName() + " finish transaction " + item.getPrices()[0] + " for " + item.getQuantity() + " units of " + item.getName());
            System.out.print("transaction refused by market");

            mutexInventory.unlock();
            return;
        }
        saveItem(item);
        if(isSeller)
            money += item.getQuantity() * item.getPrices()[0];
        else
            money -= item.getQuantity() * item.getPrices()[0];
        mutexInventory.unlock();
    }
    public final boolean ReceiveAcceptedOffer(Item item, AgentMarket agent, boolean isSeller)
    {
        System.out.println(getLocalName() + " is offered " + item.getPrices()[0] + " for " + item.getQuantity() + " units of " + item.getName());

        if(!market.VerificationTransaction(item))
            return false;
        mutexInventory.lock();
        if(!TestOffer(item, agent, true))
        {
            mutexInventory.unlock();
            return false;
        }
        saveItem(item);
        if(isSeller)
            money += item.getQuantity() * item.getPrices()[0];
        else
            money -= item.getQuantity() * item.getPrices()[0];
        mutexInventory.unlock();
        return true;
    }

    public void setCurveType(Curves curveType) {
        this.curveType = curveType;
    }
}
